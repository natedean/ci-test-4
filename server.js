const express = require('express');
const path = require('path');
const app = express();
const expressStaticGzip = require('express-static-gzip');
const PORT = process.env.PORT || 3000;

// Redirect to HTTPS if necessary
app.use(function(req, res, next) {
    if (req.get('x-forwarded-proto') === 'http') return res.redirect(`https://${req.get('host')}${req.url}`);

    next();
});

app.use(expressStaticGzip('client/build'));

// this is an old pattern, but I don't think we need it
// app.get('/', (req, res) => {
//   res.sendFile('client/build/index.html');
// });

app.listen(PORT, () => {
  console.log(`App listening on PORT ${PORT}`);
});
